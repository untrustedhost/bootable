#!/usr/bin/env bash

[ "${1:-}" ] || { echo "script requires output file path" 1>&2 ; exit 1 ; }

# generate a persistent hardware instance id
dmi_id_tree='/sys/class/dmi/id'
dmi_id_sources=('product_uuid' 'board_serial' 'chassis_serial' 'product_serial')

deliver_id () {
  local file id path
  # check dmi/id first...
  for file in "${dmi_id_sources[@]}" ; do
    path="${dmi_id_tree}/${file}"
    [ -f "${path}" ] && read -r id < "${path}"
    [ "${id}" ] && {
      echo "${id}" && return 0
    }
  done
  return 1
}

sysid="$(deliver_id)"

[ "${sysid}" ] || exit 1

echo "${sysid}" > "${1}"

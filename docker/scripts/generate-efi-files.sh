#!/usr/bin/env bash

set -eux

# create fat16 image for EFI
truncate -s 12M /boot/efiboot.img
mkfs.vfat -F16 /boot/efiboot.img

# use mtools to copy things to it
mmd -i /boot/efiboot.img efi
mmd -i /boot/efiboot.img efi/boot
mmd -i /boot/efiboot.img efi/boot/i386-efi

# standard x64 efi - shim, grub
if [[ -e /usr/lib/shim/shimx64.efi.signed ]] ; then
  mcopy -i /boot/efiboot.img /usr/lib/shim/shimx64.efi.signed ::efi/boot/bootx64.efi
elif [[ -e /boot/efi/EFI/fedora/shimx64.efi ]] ; then
  mcopy -i /boot/efiboot.img /boot/efi/EFI/fedora/shimx64.efi ::efi/boot/bootx64.efi
else
  printf "could not find signed shim" 1>&2 ; exit 1
fi

if [[ -e /usr/lib/grub/x86_64-efi-signed/gcdx64.efi.signed ]] ; then
  mcopy -i /boot/efiboot.img /usr/lib/grub/x86_64-efi-signed/gcdx64.efi.signed ::efi/boot/grubx64.efi
elif [[ -e /boot/efi/EFI/fedora/gcdx64.efi ]] ; then
  mcopy -i /boot/efiboot.img /boot/efi/EFI/fedora/gcdx64.efi ::efi/boot/grubx64.efi
else
  printf "could not find grub EFI cd loader" 1>&2 ; exit 1
fi

# ia32 efi - unsigned, modules
mkimage=grub-mkimage
type grub2-mkimage 2>&1 > /dev/null && mkimage=grub2-mkimage
$mkimage -O i386-efi -d /usr/lib/grub/i386-efi -o /usr/lib/grub/i386-efi/gcdia32.efi -p /EFI/BOOT \
  all_video boot btrfs cat chain configfile echo efifwsetup efinet ext2 fat font gfxmenu gfxterm \
  gzio halt hfsplus iso9660 jpeg loadenv linux lvm mdraid09 mdraid1x minicmd normal \
  part_apple part_msdos part_gpt password_pbkdf2 png reboot search search_fs_uuid search_fs_file \
  search_label sleep syslinuxcfg test tftp regexp video xfs

mcopy -i /boot/efiboot.img /usr/lib/grub/i386-efi/gcdia32.efi ::efi/boot/bootia32.efi
mcopy -i /boot/efiboot.img /usr/lib/grub/i386-efi/*.mod ::efi/boot/i386-efi

# copy modules for ia32 to cd directly as well
mkdir -p /EFI/BOOT/i386-efi
cp /usr/lib/grub/i386-efi/*.mod /EFI/BOOT/i386-efi

# point any EFI-CD-GRUB back to /boot/grub plz
{
  echo "insmod linux"
  echo "configfile /boot/grub/grub.cfg"
} >> "/EFI/BOOT/grub.cfg"

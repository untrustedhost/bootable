#!/usr/bin/env bash

# if you are on usb serial, configure a console service for later.
[ "${CONFIG_USBSER_INSTALL}" ] || CONFIG_USBSER_INSTALL=$(tty)

case "${CONFIG_USBSER_INSTALL}" in
  /dev/ttyUSB*) CONFIG_USBSER_INSTALL="${CONFIG_USBSER_INSTALL#/dev/ttyUSB}" ;;
  /dev/*)       unset CONFIG_USBSER_INSTALL ;;
  [0-9]*)       : ;;
  *)
    echo "CONFIG_USBSER_INSTALL should be set to the unit number of the USB serial port" 1>&2
    unset CONFIG_USBSER_INSTALL
  ;;
esac

# configure the usbcon dracut module now :)
[ -n "${CONFIG_USBSER_INSTALL}" ] && {
  echo "ttyUSB${CONFIG_USBSER_INSTALL}" > /etc/dracut-usbcon.conf
}

# for any vmlinuz file we have, (re)generate the appropriate initrd
for f in /boot/vmlinuz-* ; do
  [[ -e "${f}" ]] || { printf '%s\n' "we don't have any vmlinuz- here?" 1>&2 ; exit 1 ; }
  v="${f#/boot/vmlinuz-}"
  dracut -f "/boot/initrd.img-${v}" "${v}"
done

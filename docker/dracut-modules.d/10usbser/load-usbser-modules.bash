#!/usr/bin/bash

jam_usbser(){
  local line
  test -e "${1}" || return 0
  while read -r line ; do
    case "${line}" in
      '#'*) : ;;
      *)    modprobe "${line}" || return 0 ;;
    esac
  done < "${1}"
}

jam_usbser /etc/dracut-usbser.modules

udevadm settle --timeout=0

#!/usr/bin/env bash

check() {
  [ -f /etc/dracut-usbser.modules ] || return 1
  require_binaries grep
  return 255
}

depends() {
  return 0
}

installkernel() {
  local l
  while read -r l ; do
    instmods $l
  done < <(grep -v '^#' /etc/dracut-usbser.modules)
}

install() {
  inst_simple /etc/dracut-usbser.modules /etc/dracut-usbser.modules
  inst_hook pre-udev 30 "${moddir}/load-usbser-modules.bash" "$moddir/load-usbser-modules.bash"
  inst_simple "${moddir}/load-usbser-modules.service" "$systemdsystemunitdir/load-usbser-modules.service"
  inst_simple "${moddir}/load-usbser-modules.bash" "/usr/lib/untrustedhost/scripts/load-usbser-modules.bash"
}

#!/usr/bin/env bash

check() {
  require_binaries lspci grep cut || return 1
}

depends() {
  return 0
}

# this is most of the module, just jamming some kernel deps in
installkernel() {
  # list graphics class and get the kernel modules. add those.
  # shellcheck disable=SC2046
  local modlist
  modlist=$(lspci -nk -d ::0300 | grep modules | cut -d: -f2)
  [ "${modlist}" ] && instmods $(lspci -nk -d ::0300 | grep modules | cut -d: -f2)
  return 0
}
